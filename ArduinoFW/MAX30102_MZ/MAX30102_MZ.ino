// This example must be used in conjunction with the Processing sketch located
// in extras/rolling_graph

#include <Wire.h>
#include "MAX30102_PulseOximeter.h"


#define REPORTING_PERIOD_MS     1000

// PulseOximeter is the higher level interface to the sensor
// it offers:
//  * beat detection reporting
//  * heart rate calculation
//  * SpO2 (oxidation level) calculation
PulseOximeter pox;

#define BEAT_LED  13

#define DEBUG_ON  1

uint32_t tsLastReport = 0;
uint32_t beatLedTime = 0;

#define AVG_POINTS_HR 10
#define AVG_POINTS_OXY 10
#define AVG_POINTS_IBI 10

float currHR, oldHR, avgHR[AVG_POINTS_HR], avgHRResult;
float currOxy, oldOxy, avgOxy[AVG_POINTS_OXY], avgOxyResult;
uint16_t currIBI, oldIBI, avgIBI[AVG_POINTS_IBI], avgIBIResult;
uint8_t avgHRCnt, avgOxyCnt, avgIBICnt; 



// Callback (registered below) fired when a pulse is detected
void onBeatDetected()
{
  bool arty = 0;

#ifdef DEBUG_ON    
  Serial.println("B:1");
#endif

  currHR = pox.getHeartRate();
  currOxy = pox.getSpO2();
  currIBI = pox.getIBI();


  if ( (currIBI > avgIBIResult*1.3) || (currIBI < avgIBIResult*0.7) || (currIBI > 3000) || (currIBI < 300) ) arty = 1;    //IBI sanity check
  if ( (currHR > avgHRResult*1.3) || (currHR < avgHRResult*0.7) ||  (currHR > 200) || (currHR < 10) ) arty = 1;        //HR sanity check
  //if ( (currOxy < 10) || (currOxy > 100) )   arty = 1;   //Oxy sanity check
 
  

  if (arty == 1)
  {
   // currIBI = 0;
   // currHR = 0;
   // currOxy = 0; 
  }
  else
  {
    digitalWrite(BEAT_LED, HIGH);
    beatLedTime = millis() + 10;
  }


    avgHR[avgHRCnt] = currHR;
    
    float HRSumm = 0;
    for (uint8_t cnt = 0; cnt < AVG_POINTS_HR; cnt++)
       HRSumm += avgHR[cnt];
       
    avgHRResult = HRSumm / (float)AVG_POINTS_HR;  

    if (avgHRCnt == (AVG_POINTS_HR-1)) avgHRCnt = 0;
    else avgHRCnt++;


    avgOxy[avgOxyCnt] = currOxy;
    
    float OxySumm = 0;
    for (uint8_t cnt = 0; cnt < AVG_POINTS_OXY; cnt++)
       OxySumm += avgOxy[cnt];
       
    avgOxyResult = OxySumm / (float)AVG_POINTS_OXY;  

    if (avgOxyCnt == (AVG_POINTS_OXY-1)) avgOxyCnt = 0;
    else avgOxyCnt++;

    avgIBI[avgIBICnt] = currIBI;
    
    uint32_t IBISumm = 0;
    for (uint8_t cnt = 0; cnt < AVG_POINTS_IBI; cnt++)
       IBISumm += avgIBI[cnt];
       
    avgIBIResult = IBISumm / AVG_POINTS_IBI;  

    if (avgIBICnt == (AVG_POINTS_IBI-1)) avgIBICnt = 0;
    else avgIBICnt++;


   Serial.print(millis() );
   Serial.print("," );
   Serial.print(currHR);
   Serial.print("," );
   Serial.print(currOxy);
   Serial.print("," );
   Serial.print(currIBI);
   Serial.print("," );
   Serial.print(arty);
   Serial.print("," );
   Serial.print(avgHRResult);
   Serial.print("," );
   Serial.print(avgOxyResult);   
   Serial.print("," );
   Serial.println(avgIBIResult);      

oldHR = currHR;
oldOxy = currOxy;
oldIBI = currIBI;

    

    
}

void setup()
{
    Serial.begin(115200);
    delay(1000); //wait for logger to boot

    pinMode(BEAT_LED, OUTPUT);


    // Initialize the PulseOximeter instance and register a beat-detected callback
    // The parameter passed to the begin() method changes the samples flow that
    // the library spews to the serial.
    // Options:
    //  * PULSEOXIMETER_DEBUGGINGMODE_PULSEDETECT : filtered samples and beat detection threshold
    //  * PULSEOXIMETER_DEBUGGINGMODE_RAW_VALUES : sampled values coming from the sensor, with no processing
    //  * PULSEOXIMETER_DEBUGGINGMODE_AC_VALUES : sampled values after the DC removal filter

    // Initialize the PulseOximeter instance
    // Failures are generally due to an improper I2C wiring, missing power supply
    // or wrong target chip

#ifdef DEBUG_ON    
    if (!pox.begin(PULSEOXIMETER_DEBUGGINGMODE_PULSEDETECT)) {
        Serial.println("ERROR: Failed to initialize pulse oximeter");
        for(;;);
    }
#else
    if (!pox.begin()) 
    {
      Serial.println("ERROR: Failed to initialize pulse oximeter");
      for(;;);
    } 
    //else Serial.println("SUCCESS");

 
    Serial.println("Time,BPM,Oxi,IBI,Motion,AvgHR,avgOxy,avgIBI");

#endif    

   // pox.setIRLedCurrent(MAX30100_LED_CURR_14_2MA);

    pox.setOnBeatDetectedCallback(onBeatDetected);
}

void loop()
{
    // Make sure to call update as fast as possible
    pox.update();

    if (millis() >= beatLedTime)
    {
      digitalWrite(BEAT_LED, LOW);
      beatLedTime = 0xFFFFFFFF;
    }

#ifdef DEBUG_ON  
    // Asynchronously dump heart rate and oxidation levels to the serial
    // For both, a value of 0 means "invalid"
    if (millis() - tsLastReport > REPORTING_PERIOD_MS) {
        Serial.print("H:");
        Serial.println(pox.getHeartRate());

        Serial.print("O:");
        Serial.println(pox.getSpO2());

        Serial.print("Z:");
        Serial.println(pox.getIBI());

        tsLastReport = millis();
    }
#endif    
}
